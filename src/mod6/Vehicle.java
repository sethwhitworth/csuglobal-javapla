/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mod6;

/**
 *
 * @author sethwhitworth
 */
/**
 * Vehicle class This class is used to create Vehicle objects.
 */
public class Vehicle {
    // Vehicle class instance fields
    @SuppressWarnings("unused")
    private boolean moving;
    // whether or not the vehicle is currently moving
    private double speed;
    // speed in mph of the vehicle
    private char bearing;

    // direction vehicle headed ('N','E','S', or 'W')
    public Vehicle()
    // Vehicle class no-arg constructor
    {
        moving = false;        // assume not moving
        speed = 0.0;        // not moving
        bearing = 'N';        // assume 'N'orth
        System.out.println("Created a vehicle (no-arg)");
    }

    public Vehicle(double initialSpeed)
    // Vehicle 1-argconstructor
    {
        bearing = 'W';
        speed = initialSpeed;
        if (speed > 0.0) {
            moving = true;
        }
        System.out.println("Created a vehicle (1-arg)");
    }

    public Vehicle(double initialSpeed, char initialBearing)
    // Vehicle 2-arg constructor
    {
        bearing = initialBearing;
        speed = initialSpeed;
        if (speed > 0.0) {
            moving = true;
        }
        System.out.println("Created a vehicle (2-arg)");
    }

    public void start(double initialSpeed, char initialBearing) {
        moving = true;
        if (initialSpeed >= 5.0 && initialSpeed <= 20.0) {
            speed = initialSpeed;            // valid expected range
        } else if (initialSpeed >= 0.0 && initialSpeed < 5.0) {
            speed = 5.0;            // minimum
        } else if (initialSpeed < 0.0) {
            speed = 0.0;            // assume no movement
            moving = false;
        } else if (initialSpeed > 20.0) {
            speed = 20.0;            // maximum allowed
        }
        switch (initialBearing) {
        case 'N':
            bearing = initialBearing;
            break;
        case 'E':
            bearing = initialBearing;
            break;
        case 'S':
            bearing = initialBearing;
            break;
        case 'W':

            bearing = initialBearing;
        default:
            System.out.println("invalid bearing " + initialBearing
                    + " set to N");            // additionaluser notification
            bearing = 'N';
        }
    }

    public double getSpeed()
    // get and return current speed in mph
    {
        return speed;
    }

    public void setSpeed(double newSpeed)
    // set new speed in mph
    {
        speed = newSpeed;
    }

    public char getBearing()
    // get and return current bearing
    {
        return bearing;
    }

    public void setBearing(char bearing)
    {
        this.bearing = bearing;
    }

    public void speedUp(double mphSteps, int numSteps) {
        int counter = 0;
        while (counter < numSteps) {
            speed += mphSteps;
            System.out.println("counter= " + counter + ", " + this.toString());
            counter++;
        }
    }

    public String toString() {
        return "From toString(): speed= " + getSpeed() + " mph and bearing= "
                + getBearing();
    }
}

