/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mod6;

/**
 *
 * @author sethwhitworth
 */
/**
 * Car class This class is used to create Car objects and inherits from the
 * Vehicle class.
 */
public class Car extends Vehicle {
    // Car class instance fields
    private String color;    // color of the Car
    private int doors;    // number of doors of the Car
    private double hp;

    public Car() {
        super();
        color = "red";
    }
    // engine horsepower
    public Car(String carColor, int numDoors, double horsePower,
            double startingSpeed)
    // 4-arg constructor
    {
        super(startingSpeed);
        color = carColor;
        doors = numDoors;
        hp = horsePower;
        System.out.println("Created a car");
    }
    
    public void start(double initialSpeed, char initialBearing) {
        
        //We must force the car to start at 0 mph
        setSpeed(0);
        
        //We must force the car to start in a southernly direction
        setBearing('S');
    }
    
    public void speedUp(double mph) {
        //this class always accelerates the vehicle by 50mph
        super.speedUp(50, 1);
    }    

    public String getColor() {
        return color;
    }

    public int getDoors() {
        return doors;
    }

    public double getHp() {
        return hp;
    }

    public String toString() {
        return "From Car toString(): color= " + getColor() + " doors= "
                + getDoors() + " hp= " + getHp() + " speed= " + getSpeed()
                + " mph and bearing= " + getBearing();

    }
}

