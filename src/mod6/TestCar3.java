/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mod6;

/**
 *
 * @author sethwhitworth
 */
import java.util.Random;

/**
 * TestCar3 class is derived from TestCar2. This class is used to create a Car, start it up, and then
 * speed it up.
 */
public class TestCar3 {
    public static void main(String[] args) {
        // create, start, and speed up a Car
        Car myCar3 = new Car("red", 4, 300.0, 10.0);

        myCar3.start(0, 'S');
        System.out.println(myCar3.toString());
        myCar3.speedUp(50);
              
    }
}
