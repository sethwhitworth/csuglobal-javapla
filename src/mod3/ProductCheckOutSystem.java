/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mod3;

/**
 *
 * @author Seth
 */
import javax.swing.JOptionPane;

//Delcare a Product class. This will store the 
class Product {
    //declare the Product variables
    private int _id;
    private double _price;

    //declare initalization function when called must pass id and price, which will be set and stored)
    public Product(int id, double price) {
        _id = id;
        _price = price;
    }
    
    //function used to get the price
    public double getPrice() {
        return _price;
    }
}

public class ProductCheckOutSystem 
{
    //declare an array of products
    Product[] products;
    
    //declaration class
    public ProductCheckOutSystem() {
        //setup the Products variable and then load all of the different products into the system
        products = new Product[5];
        products[0] = new Product(1, 2.98);
        products[1] = new Product(2, 4.5);
        products[2] = new Product(3, 9.98);
        products[3] = new Product(4, 4.49);
        products[4] = new Product(5, 6.87);
    }
    
    public void checkout() {
        //declare the string that we will use to collect information
        StringBuilder message = new StringBuilder("Product  Quantity    Line Cost   Order Cost").append("\n");
        //establish the while loop, this will run as long as a return is not set
        while (true) {
            String s;
            s = JOptionPane.showInputDialog("Enter Product No.(1-5) or -1 to Quit");
            int id = Integer.parseInt(s);
            
            // if a -1 is sent terminate processing
            if (id == -1)
                return;
            
            //collect the quantity and again if they pass a -1 end application.
            s = JOptionPane.showInputDialog("Enter Quantity or -1 to Quit");
            int quantity = Integer.parseInt(s);
            if (quantity == -1)
                return;
            //establish the template for variables to be sent to the user
            String template = "     %s              %s                 %s             %s";
            message.append(String.format(template, String.valueOf(id), String.valueOf(quantity),
                                           String.valueOf(products[id - 1].getPrice()),
                                           String.valueOf(products[id - 1].getPrice() * quantity))).append("\n");
            JOptionPane.showMessageDialog(null, message.toString(), "Order Subtotal Amount", JOptionPane.INFORMATION_MESSAGE);         
            }
    }
    
    //function that is run after compile
    public static void main(String[] args) {
        ProductCheckOutSystem system = new ProductCheckOutSystem();
        
        //run the system
        system.checkout();
    }
}
