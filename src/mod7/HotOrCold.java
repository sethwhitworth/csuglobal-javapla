/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mod7;

/**
 *
 * @author Seth
 */
import java.awt.Color;
import java.awt.Font;
import java.util.Random;

@SuppressWarnings("serial")

//delcare the class and extend the jFrame class. We use jFrame to generate the output
public class HotOrCold extends javax.swing.JFrame {
    
    //declare the variables we will need throughout the application
    int num, source, guess, lastguess;
    private final Color c;
    Random rand = new Random();

    //declare the form features that we will use
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextField1;

    /**
     * Creates new form NumberGuessing
     */
    public HotOrCold() {
        
        //init the components and set the game labels to hidden 
        initComponents();
        jLabel1.setVisible(false);
        jLabel2.setVisible(false);
        c = this.getBackground();
        
        //disable the text field until the user starts the game
        jTextField1.setEditable(false);

        jPanel1.setBackground(Color.LIGHT_GRAY);

    }

    private void initComponents() {

        //establish the form components. 
        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Guessing Game");

        jButton1.setText("Start Game");

        //declare the listener for the button. When clicked this will be called
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startGame(evt);
            }
        });

        //declare the listener for the textBox. When the user clicks enter this will be called
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tryGuess(evt);
            }
        });
        
        //setup and build the jPanel. the following code establishes the layout
        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jButton1)
                                        .addGap(38, 38, 38)
                                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jLabel2))
                        .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jButton1)
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2)
                        .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
        );

        pack();
    }

    private void startGame(java.awt.event.ActionEvent evt) {
        
        //when the user clicks the start button set the number of guesses to 0
        //select the random number
        num = 0;
        source = rand.nextInt(1000) + 1;
        lastguess = 0;

        jLabel1.setText("I have a number between 1 and 1000 can you guess my number?");
        jLabel2.setText("Please enter a number for your first guess and then hit Enter");

        jLabel1.setVisible(true);
        jLabel2.setVisible(true);

        jButton1.setEnabled(false);
        jTextField1.setEditable(true);

        jPanel1.setBackground(Color.LIGHT_GRAY);

        //print the random number for debugging purposes
        System.out.println(source);
        return;
    }

    private void tryGuess(java.awt.event.ActionEvent evt) {
        
        //try to see how the guess plays out
        num++;
        jLabel1.setFont(new Font("Times New Roman", Font.BOLD, 14));
        jLabel2.setVisible(false);
        
        //get the guess from the text field
        guess = Integer.parseInt(jTextField1.getText());
        
        //see if the guess is correct
        if (guess == source) {
            jLabel1.setText(guess + " is CORRECT! Total guesses was " + (num) + "." + " Click the button to Start a New Game");
            jPanel1.setBackground(Color.GREEN);
            jTextField1.setEditable(false);
            jButton1.setEnabled(true);
            
            jTextField1.setText("");
            return;
        }
        
        //Check if this is the initial guess. The initial guess is always considered Warmer
        if (num == 1) {
            if (guess < source) {
                jLabel1.setText(guess + " is TOO LOW! But you getting WARMER! Enter guess number " + (num + 1) + ".");
            }
            if (guess > source) {
                jLabel1.setText(guess + " is TOO HIGH! But you getting WARMER! Enter guess number " + (num + 1) + ".");
            }
            jPanel1.setBackground(Color.RED);
            lastguess = guess;
        } else {
            //determine if they are warmer or colder by looking at the difference
            //between the last guess and source and this guest and source
            int guessdif = Math.abs(guess - source);
            int lastguessdif = Math.abs(lastguess - source);
            if (guess < source) {
                if (guessdif > lastguessdif) {
                    jLabel1.setText(guess + " is TOO LOW! But you getting COLDER! Enter guess number " + (num + 1) + ".");
                    jPanel1.setBackground(Color.BLUE);
                }
                if (guessdif < lastguessdif) {
                    jLabel1.setText(guess + " is TOO LOW! But you getting WARMER! Enter guess number " + (num + 1) + ".");
                    jPanel1.setBackground(Color.RED);
                }
            }
            if (guess > source) {
                if (guessdif > lastguessdif) {
                    jLabel1.setText(guess + " is TOO HIGH! But you getting COLDER! Enter guess number " + (num + 1) + ".");
                    jPanel1.setBackground(Color.BLUE);
                }
                if (guessdif < lastguessdif) {
                    jLabel1.setText(guess + " is TOO HIGH! But you getting WARMER! Enter guess number " + (num + 1) + ".");
                    jPanel1.setBackground(Color.RED);
                }

            }
            lastguess = guess;
        }
        
        jTextField1.setText("");
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new HotOrCold().setVisible(true);
            }
        });
    }

}
