/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mod5;

import java.util.Arrays;

/**
 *
 * @author Seth
 */
public class ArraySearching {
    
    //Initial array
    int[] array = { 23, 17, 5, 90, 12, 44, 38, 84, 77, 3, 66, 55, 1, 19, 37,
            88, 8, 97, 25, 50, 75, 61, 49 };
    
    public void displayUnsortedValues() {
        
        //Print the original array as it stands. The Arrays.toString(var) will automatically parse the array to a string
        
        System.out.println("Output 1: Initial Array");
        System.out.println(Arrays.toString(array));
    }
    
    public void sequentialSearch() {
        
        // run the sequential search
        int total = 0;
        
        //declare the search paramaters in an array
        int[] keywords = {25, 30, 50, 75, 92};
        
        System.out.println("Output 2: Sequential Search for Position");
        
        //prepare to search for the variables. Use for loops to itterate through the array
        //where not possible return -1 for the value. Print a line each time. Store the total searches in total 
        for (int i = 0; i < keywords.length; i++) {
            int position = -1;
            for (int j = 0; j < array.length; j++) {
                total++;
                if (keywords[i] == array[j]) {
                    position = j;
                    break;
                }
            }
            System.out.println("Position of " + keywords[i] + " is : " + position+1 + " Number of Searches: " + position);
        }
        System.out.println("Output 3: Total number of searches for positions:  \n" + total);
    }
    
    public void standardSort() {
        Arrays.sort(array);
        System.out.println("Output 4: Sort the array using Arrays.sort(var)");
        System.out.println(Arrays.toString(array));
    }
    
    public void binarySearch() {
        
        //perform a binary search
        int total = 0;
        
        //intalize the standard search
        int[] keywords = {25, 30, 50, 75, 92};
        System.out.println("Output 5: : Binary search");   
        
        //using the for loop perform a binary search 
        for (int i = 0; i < keywords.length; i++) {
            int l = 0;
            int r = array.length - 1;
            int position = -1;
            while (l <= r) {
                total++;
                int m = (l + r) / 2;
                if (array[m] < keywords[i])
                    l = m + 1;
                else
                    if (array[m] > keywords[i])
                        r = m - 1;
                    else {
                        position = m;
                        break;
                    }
            }
            System.out.println("Position of " + keywords[i] + " is : " + position+ 1);
        }
        System.out.println("Output 6: Total number of searches for sorted array: \n" + total);
    }


    public static void main(String[] args) {
        ArraySearching driver = new ArraySearching();
        driver.displayUnsortedValues();
        driver.sequentialSearch();
        driver.standardSort();
        driver.binarySearch();
    }
}
