/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mod2;

/**
 *
 * @author Seth
 */
public class BankAccount {
    private double balance;// Account balance
    private double interestRate;  // Interest rate
    private double annualInterestRate;    // Annual interest rate
    private double interest;

    // Interest earned
    /**
     * The constructor initializes the balance and interestRate fields with the
     * values passed to startBalance and intRate. The interest field is assigned
     * to 0.0.
     */
    
    public BankAccount(double startBalance, double annualIntRate) { //collect the annual intrest rate.
        balance = startBalance;
        annualInterestRate = annualIntRate;
	   interestRate = annualIntRate/12.0/100.0;    //Perform the math to convert the APR into a monthly intrest rate    			   
           interest = 0.0;
    }

    /**
     * The deposit method adds the parameter amount to the balance field.
     */
    public void deposit(double amount) {
        balance += amount;
    }

    /**
     * The withdraw method subtracts the parameter amount from the balance
     * field.
     */

    public void withdraw(double amount) {
        balance -= amount;
    }

    /**
     * The addInterest method adds the interest for the month to the balance
     * field.
     */
    public void addInterest() {
        interest = balance * interestRate;
        balance += interest;
    }

    /**
     * The getBalance method returns t he value in the balance field.
     */
    public double getBalance() {
        return balance;
    }

    /**
     * The getInterest method returns the value in the interest field.
     */
    public double getInterest() {
        return interest;
    }

    public double getAnnualInterestRate() {
        return annualInterestRate;
    }

    public void setAnnualInterestRate(double annualInterestRate) {
        this.annualInterestRate = annualInterestRate;
    }
}
